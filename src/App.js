import React from 'react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./components/Home";
import Article from "./components/Article";
import {Container, Row} from "reactstrap";
import {Route, Switch} from 'react-router-dom';

function App() {
    return (
        <Container>
            <Row>
                <Header/>
            </Row>
            <Row>
                <Switch>
                    <Route path="/article/:id" component={Article}/>
                    <Route path="/" exact component={Home}/>
                </Switch>
            </Row>
            <Row>
                <Footer/>
            </Row>
        </Container>
    );
}

export default App;
