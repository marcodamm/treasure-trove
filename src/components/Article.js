import React, {Component} from 'react';
import {Row, Col} from "reactstrap";
import moment from "moment";
import {createMarkup, sanitizeHTML} from "../utilities/sanitizer";
import ActionButton from "./UI/ActionButton";

class Article extends Component {
    constructor(props) {
        super(props);
        this.state = {
            article: null
        };
    }

    componentDidMount() {

        const headers = new Headers();
        //const apiUrl = 'http://localhost:3001/hits/'
        const apiUrl = 'https://api.nunux.org/keeper/v2/documents/';
        headers.append('Authorization', 'Basic ' + btoa('api:a1b95acce91bb1885bac5c3b55474d039c205633'));

        if (this.props.match.params.id) {
            if (!this.state.article || (this.state.article && this.state.article.id !== this.props.match.params.id)) {
                fetch(apiUrl + this.props.match.params.id, {headers: headers})
                    .then(response => response.json())
                    .then(data => {
                        console.log(data);
                        this.setState({article: data});
                    })
                    .catch(error => console.error(error))
            }
        }
    }

    render() {
        let article = <Row><Col className="d-flex justify-content-center align-items-center vh-100"><h4>Loading your article...</h4></Col></Row>;

        if (this.state.article) {
            article = (
                <Row className="my-3">
                    <Col xs={12}>
                        <h4>{this.state.article.title}</h4>
                        <p dangerouslySetInnerHTML={createMarkup(sanitizeHTML(this.state.article.content, ['p', 'br', 'strong', 'code']))} />
                        <p className="small text-muted">
                            This article was added on {moment(this.state.article.date).format('DD.MM.YYYY')}
                        </p>
                        <ActionButton link={this.state.article.origin} />
                    </Col>
                </Row>
                );
        }

        return (
            <Col>
                {article}
            </Col>
        );
    }
}

export default Article;
