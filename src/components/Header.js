import React from 'react';
import {Col, Jumbotron} from "reactstrap";

const Header = () => {

    return (
        <Col>
            <Jumbotron className="mb-0">
                <h1 className="display-5">Marco Damm</h1>
                <p className="lead">A place for my notes, views and ideas. All posts are ordered in order of
                    their publication date. For my and your reference an approximate time to read is also
                    provided.</p>
            </Jumbotron>
        </Col>
    )
};

export default Header;
