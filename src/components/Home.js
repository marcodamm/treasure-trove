import React, {Component} from 'react';
import {Row, Col, Card, CardBody, CardColumns} from "reactstrap";
import {Link} from 'react-router-dom';
import moment from "moment";
import {getHostname, sanitizeHTML, shorten, timeToRead} from "../utilities/sanitizer";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            comments: [],
            labels: []
        };
    }

    componentDidMount() {

        const bookmarksUrl = 'https://api.nunux.org/keeper/v2';
        //const bookmarksUrl = 'http://localhost:3002';
        const commentsUrl = 'http://localhost:3001';
        const headers = new Headers();

        headers.append('Authorization', 'Basic ' + btoa('api:a1b95acce91bb1885bac5c3b55474d039c205633'));

        //fetch(apiUrl + '/documents', {headers: headers})
        fetch(bookmarksUrl + '/documents', {headers: headers})
            .then(response => response.json())
            .then(data => {
                const sortedByDate = data.hits.reverse();
                this.setState({articles: sortedByDate});

                fetch(commentsUrl + '/comments', {headers: headers})
                    .then(response => response.json())
                    .then(data => {
                        console.log('comments', data);

                        this.setState({comments: data});
                    })
                    .catch(error => console.error(error))

            })
            .catch(error => console.error(error))

    }

    render() {

        let articles = <Row><Col className="d-flex justify-content-center align-items-center vh-100"><h4>Loading your article...</h4></Col></Row>;

        if (this.state.articles) {
            articles = this.state.articles.map((article) =>
                <Card to={'./article/' + article.id} key={article.id} tag={Link} className="my-3 shadow-sm text-dark" color="info">
                    <CardBody>
                        <h4>{article.title}</h4>
                        <div>
                            <p>{sanitizeHTML(shorten(article.content, 180, ' '), [])}</p>
                            <span className="small">
                                {moment(article.date).format('MMMM YYYY')}&nbsp;|&nbsp;
                                {getHostname(article.origin)}&nbsp;|&nbsp;
                                {timeToRead(article.content)}
                            </span>
                        </div>
                    </CardBody>
                </Card>
            );
        }

        return (
            <Col>
                <CardColumns>
                    {articles}
                </CardColumns>
            </Col>
        );
    }
}

export default Home;