import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLink, faCircle } from '@fortawesome/free-solid-svg-icons'

const ActionButton = (props) => {

    const iconStyle = {
        position: 'fixed',
        bottom: '20px',
        right: '20px',
        filter: 'drop-shadow(5px 5px 6px rgba(0,0,0,0.25)'
    };

    return (
        <a href={props.link} className="fa-layers fa-fw fa-4x" style={iconStyle}>
            <FontAwesomeIcon icon={faCircle}/>
            <FontAwesomeIcon icon={faLink} inverse transform="shrink-10"/>
        </a>
    )
};

export default ActionButton;
