import sanitizer from "sanitize-html";

// Sanitize HTML
export const sanitizeHTML = (
    HTMLString,
    allowedTags = [
        'a',
        'b',
        'blockquote',
        'br',
        'code',
        'em',
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6',
        'hr',
        'i',
        'img',
        'li',
        'nl',
        'ol',
        'p',
        'pre',
        'strike',
        'strong',
        'table',
        'tbody',
        'td',
        'th',
        'thead',
        'tr',
        'ul'
    ],
    allowedAttributes = {
        img: ['src']
    }) => {
    const clean = sanitizer(HTMLString, {
        allowedTags: allowedTags,
        allowedAttributes: allowedAttributes
    });

    return clean;
};

// Shorten a string to less than maxLen characters without truncating words.
export const shorten = (str, maxLen, separator = ' ') => {
    if (str.length <= maxLen) return str;
    return str.substr(0, str.lastIndexOf(separator, maxLen)) + '...';
}

// Create HTML-Markup
export const createMarkup = (input) => {
    return {__html: input};
};

// Extract Hostname from full URL
export const getHostname = (fullURL) => {
    let url = fullURL ? new URL(fullURL) : 'https://example.com';
    return url.hostname;
};

// Count words
export const timeToRead = (text) => {
    const textLength = text.split(' ').length;
    const wordsPerMinute = 233;
    let timeToRead = textLength / wordsPerMinute;
    timeToRead = +timeToRead.toFixed(0);
    console.log(timeToRead);

    if (timeToRead === 0 || timeToRead === 1) {
        return '1 minute';
    } else {
        return timeToRead + ' minutes';
    }
};
